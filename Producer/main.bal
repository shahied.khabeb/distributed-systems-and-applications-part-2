import ballerina/io;
import ballerinax/kafka;
import ballerina/sql;
import ballerinax/java.jdbc;
import ballerina/log;


function connectToDatabase() returns jdbc:Client|error {
    return new jdbc:Client("jdbc:h2:file:./target/bbes/java_jdbc" + server.toString(), User, Password);
}

function Records(kafka:ConsumerRecord kafkaRecord) returns json|error {
    string collection = check string:fromBytes(kafkaRecord.value);
    return collection.fromJsonString();
}

function CheckedRecords(kafka:Caller caller, kafka:ConsumerRecord[] records) returns json[]|error {
    json[] Information = [];
    
    foreach int i in 0 ... records.length() - 1 {
        Information[i] = check Records(records[i]);
    }



string bootstrapServer = "localhost:9093";
string User = "admin";
string Password = "password";
int server = 0;
string Path = "./target/Objects";


function getKafkaProducer() returns (kafka:Producer)|error {
    kafka:ProducerConfiguration producerConfiguration = {
        clientId: "server" + server.toString()
    };
    return new kafka:Producer(bootstrapServer, producerConfiguration);
}

   
    kafka:Error? commitResult = caller->commit();

    if commitResult is error {
        log:printError("Error connection failed ", 'error = commitResult);
    }

    return Information;
}

function writeJson(string path, string jsonString) returns error? {
    json x = check jsonString.fromJsonString();
    int y = jsonString.toBytes().length();
   
    if y < 30000000 {
        check io:fileWriteJson(path, x);
    } else {
        return error("JSON file <= 30 MB");
    }
}

function readObject(string path) returns error|json {
    return io:fileReadJson(path);
}

function writefileObject(WriteOject Data) returns error? {
    jdbc:Client uv = check connectToDatabase();

   
    string destination = Path + "/" + Data.'key + ".json";
    check writeJson(destination, Data.object);
    log:printInfo(string `Wrote ${Data.'key} to file ${destination}`);

    
    _ = check uv->execute(`
        INSERT INTO Files (Key, path)
        VALUES (${Data.'key}, ${destination}) 
    `);
    log:printInfo(string `Added file key "${Data.'key}" to database`);

}

function readFileObject(ReadObject asked) returns error? {
    log:printInfo("Received request for file: " + asked.'key);
    
    jdbc:Client uv = check connectToDatabase();
    stream<SQLObject, error?> resultStream = uv->query(`SELECT * FROM Files WHERE fileKey=${asked.'key} LIMIT 1`);
    record {|SQLObject value;|}|error? result = resultStream.next();
    if result is record {|SQLObject value;|} {
        SQLObject Data = result.value;
        log:printInfo("Found file " + asked.'key + " at " + Data.path);

      
        json|error img = readObject(Data.path);
        if img is json {
          
            FileObject texted = {
                'key: Data.fileKey,
                server: server,
                object: img.toJsonString()
            };
            kafka:Producer kafkaProducer = check getKafkaProducer();
            check kafkaProducer->send({
                'key: Data.fileKey.toBytes(),
                topic: "responses",
                value: texted.toJsonString().toBytes()
            });
            log:printInfo("Return data " + asked.'key);
        } else {
            return error("Error while reading JSON file from destination " + Data.path);
        }
    } else {
        
        return error("Unable to retrive key " + asked.'key);
    }
}

function updating(json texted) returns error? {
    log:printInfo("Update completed: " + texted.toString());
  
}

function Writingfile(kafka:Caller caller, kafka:ConsumerRecord[] records) returns error? {
    json[] Information = check CheckedRecords(caller, records);
    foreach var CheckedRecords in Information {
        writeObject Data = check CheckedRecords.cloneWithType(WriteOject);
        check writeJson(Data);
    }
}

function ReadingFile(kafka:Caller caller, kafka:ConsumerRecord[] records) returns error? {
    json[] Information = check CheckedRecords(caller, records);
    foreach var processed in Information {
        ReadObject request = check processed.cloneWithType(ReadObject);
        check readFileObject(request);
    }
}

function UpdatingFiles(kafka:Caller caller, kafka:ConsumerRecord[] records) returns error? {
    json[] processed = check CheckedRecords(caller, records);
    foreach var L in processed {
        check updating(processed);
    }
}

