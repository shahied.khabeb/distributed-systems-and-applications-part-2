public type WriteOject record {
    string key;
    string object;
    int server;
}
public type ReadObject record {
    string key;
}
public type FileObject record {
    string key;
    string object;
    int server;
}
public type SQLObject record {
    string fileKey;
    string path;
}
