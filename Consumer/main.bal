import ballerinax/kafka;
import ballerina/log;
import ballerina/lang.runtime;


string bootstrapServer = "localhost:9093";

kafka:ProducerConfiguration producerConfiguration = {
    clientId: "client-producer",
    acks: "all",
    retryCount: 3
};

kafka:Producer kafkaProducer = check new (bootstrapServer, producerConfiguration);

function Records(kafka:ConsumerRecord kafkaRecord) returns json|error {
    string collection = check string:fromBytes(kafkaRecord.value);
    return collection.fromJsonString();
}

function Recordschecked(kafka:Caller caller, kafka:ConsumerRecord[] records) returns json[]|error {
    json[] Information = [];
    
    foreach int i in 0 ... records.length() - 1 {
        Information[i] = check Records(records[i]);
    }

    kafka:Error? commitResult = caller->commit();

    if commitResult is error {
        log:printError("Error connection ", 'error = commitResult);
    }

    return Information;
}

function writeFile(WriteJson message) returns error? {
    check kafkaProducer->send({
        key: message.'key.toBytes(),
        topic: "Write_request",
        value: message.toJsonString().toBytes()
    });
}

function readFile(ReadJson message) returns error? {
    check kafkaProducer->send({
        key: message.'key.toBytes(),
        topic: "Read_requests",
        value: message.toJsonString().toBytes()
    });


}

listener kafka:Listener kafkaFileDataListener = new (bootstrapServer, {
    groupId: "ConsumerGroup",
    topics: ["responses"],
    pollingInterval: 1,
    autoCommit: false
});

function processFileDataTopic(kafka:Caller caller, kafka:ConsumerRecord[] records) returns error? {
    json[] processedRecords = check Recordschecked(caller, records);
    foreach var processedRecord in processedRecords {
        Data checked = check processedRecord.cloneWithType(Data);
        log:printInfo("Received file contents message: " + checked.toString());
    }
}

service kafka:Service on kafkaFileDataListener {
    remote function onConsumerRecord(kafka:Caller caller, kafka:ConsumerRecord[] records) returns error? {
        error? ret = processFileDataTopic(caller, records);
        if ret is error {
            log:printError("[topic:responses]: ", ret);
        }
    }
}

public function main() returns error? {
    string key = "File";
    log:printInfo("Writing to server:");
    json fieldcollection = {name: "Leo", age: 25};
    check writeFile({
        'key: key, 
        object:  fieldcollection.toJsonString()
    });
    log:printInfo("Reading to server");

    log:printInfo("Waiting for response in  5.5s");
    runtime:sleep(5.5);

    log:printInfo("Seeking files");
    check readFile({'key: key});
    check kafkaProducer->'flush();
}
