public type WriteJson record {
    string key;
    string object;
    int server;
}

public type ReadJson record {
    string key;
}

public type Data record {
    string key;
    string object;
   
}
